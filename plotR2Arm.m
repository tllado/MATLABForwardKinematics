% Calculates and plots forward kinematics of Robonaut 2's arm using DH
% parameters.
function plotR2Arm()

% Drawing of simple end effector
EndEff = [  0       0       0       ;
            0       0       0.05    ;
            -0.05   0       0.05    ;
            -0.05   0       0.1     ;
            -0.05   0       0.05    ;
            0.05    0       0.05    ;
            0.05    0       0.1     ];

% Drawing of simple R2 torso
Torso = [   0.1905  0       0       ;
            0.279   0       0.559   ;
            -0.279  0       0.559   ;
            -0.1905 0       0       ;
            0.1905  0       0       ];

EndEffT = zeros(size(EndEff));  % pre-allocate

O = [0 0 0];    % specify origin

% R2 arm joint angles
J0 = deg2rad(-50);
J1 = deg2rad(-80);
J2 = deg2rad(105);
J3 = deg2rad(-140);
J4 = deg2rad(-80);

% R2 arm DH Parameters
%               th      d       a       al
DHParams = [    pi/2    0.559   0       3*pi/2  ;   % fake, sim torso
                0       0.279   0       pi      ;   % fake, sim torso
                0       0.279   0       0       ;   % fake, sim torso
                J0-pi/2 0.279   0       pi/2    ;   % J0
                J1      0       0       -pi/2   ;   % J1
                J2      0.356   0.0508  pi/2    ;   % J2
                J3      0       0       -pi/2   ;   % J3
                J4      0.356   0.0508  0       ];  % J4

[n, ~] = size(DHParams);    % num of DH param sets
x = zeros(n + 1, 3);        % num of points to plot

% Project forward kinematics of arm joints
for ii = 1:n
    x(ii + 1, :) = FK(O, DHParams(1:ii, :))';
end
% Project forward kinematics of end effector
for ii = 1:length(EndEff)
    EndEffT(ii, :) = FK(EndEff(ii, :), DHParams())';
end

% Plot arm
plot3(x(5:end, 1), x(5:end, 2), x(5:end, 3), 'b-o', 'LineWidth', 3);
hold on;
% Plot end effector
plot3(EndEffT(:, 1), EndEffT(:, 2), EndEffT(:, 3), 'r', 'LineWidth', 3);
% Plot torso
plot3(Torso(:, 1), Torso(:, 2), Torso(:, 3), 'k', 'LineWidth', 3);
% Mirror arm
plot3(-x(5:end, 1), x(5:end, 2), x(5:end, 3), 'b-o', 'LineWidth', 3);
% Mirror end effector
plot3(-EndEffT(:, 1), EndEffT(:, 2), EndEffT(:, 3), 'r', 'LineWidth', 3);
% Format and label plot
axis([-1 1 -1 1 -0.5 1.5]);
xlabel('x');
ylabel('y');
zlabel('z');
hold off;
