% rad2deg()

% Converts radian values to degrees.

function deg = rad2deg(rad)
deg = rad*180/pi;

% end of file