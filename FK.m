% FK()
% 
% This file is part of the forward kinematics package
% Author: Travis Llado, travis@travisllado.com
% Last modified: 2017.03.30
% 
% Takes input of an origin coordinate and a list of DH parameters, then
% returns the resultant coordinate.
% 
% DH Parameters must be in the format
% DH =  [   th1 d1  a1  al1 ]
%       [   th2 d2  a2  al2 ]
%       [   ... ... ... ... ]

function EE = FK(O, DH)

[n, ~] = size(DH);  % num of DH param sets

T = eye(4); % pre-allocate

for ii = 1:n
    T = T*DHMatrix(DH(ii, :)); % add next step to transform
end

EE = T*[O 1]';  % end eff. position, ...
EE = EE(1:3);   % ... minus an unnecessary '1' at the end

% end of file