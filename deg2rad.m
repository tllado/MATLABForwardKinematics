% deg2rad()

% Converts degree values to radians.

function rad = deg2rad(deg)
rad = deg*pi/180;

% end of file