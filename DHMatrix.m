% DHMatrix()
% 
% This file is part of the forward kinematics package
% Author: Travis Llado, travis@travisllado.com
% Last modified: 2017.03.30
% 
% Takes input of a single set of Denavit-Hartenberg parameters and returns
% the standard DH matrix for those parameters.

function A = DHMatrix(param)

th = param(1);
d =  param(2);
a =  param(3);
al = param(4);

% The long way
%{
R_th = [    cos(th) -sin(th)    0   0   ;
            sin(th) cos(th)     0   0   ;
            0       0           1   0   ;
            0       0           0   1   ];

Z_d = [     1   0   0   0   ;
            0   1   0   0   ;
            0   0   1   d   ;
            0   0   0   1   ];

Z_a = [     1   0   0   a   ;
            0   1   0   0   ;
            0   0   1   0   ;
            0   0   0   1   ];

R_al = [    1   0       0           0   ;
            0   cos(al) -sin(al)    0   ;
            0   sin(al) cos(al)     0   ;
            0   0       0           1   ];

A = R_th*Z_d*Z_a*R_al;
%}

% The short way
A = [   cos(th) -sin(th)*cos(al)    sin(th)*sin(al)     a*cos(th)   ;
        sin(th) cos(th)*cos(al)     -cos(th)*sin(al)    a*sin(th)   ;
        0       sin(al)             cos(al)             d           ;
        0       0                   0                   1           ];

% end of file